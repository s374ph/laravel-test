<?php

namespace App\Factories;

use App\Interfaces\PaymentProviderInterface;
use App\PaymentProviders\ApplePaymentProvider;

class ApplePaymentProviderConcrete extends PaymentProviderFactory
{
    /**
     * @return PaymentProviderInterface
     */
    public function getPaymentProvider(): PaymentProviderInterface
    {
        return new ApplePaymentProvider();
    }
}
