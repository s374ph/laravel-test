<?php

namespace App\Factories;

use App\Interfaces\PaymentProviderInterface;

abstract class PaymentProviderFactory
{
    abstract public function getPaymentProvider(): PaymentProviderInterface;
}
