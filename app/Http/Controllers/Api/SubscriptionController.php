<?php

namespace App\Http\Controllers\Api;

use App\Factories\ApplePaymentProviderConcrete;
use App\Http\Controllers\Controller;
use App\Services\SubscriptionService;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class SubscriptionController extends Controller
{
    /**
     * @param SubscriptionService $subscriptionService
     * @param string $provider
     */
    public function handleProvider(SubscriptionService $subscriptionService, string $provider)
    {
        try {
            if (!in_array($provider, SubscriptionService::ALLOWED_PROVIDERS, true)) {
                throw new BadRequestHttpException('Unsupported provider');
            }

            if (SubscriptionService::PROVIDER_APPLE === $provider) {
                $subscriptionService->handleSubscription(
                    new ApplePaymentProviderConcrete(),
                    request()->post()
                );
            }

        } catch (\ErrorException $e) {
            // catch something
        }
    }
}
