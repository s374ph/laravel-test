<?php

namespace App\Interfaces;

use App\Models\Transaction;

interface PaymentProviderInterface
{
    public function getTransactionFromData(array $data): Transaction;
}
