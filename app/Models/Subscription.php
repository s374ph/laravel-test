<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Subscription extends Model
{
    use HasFactory;

    const STATUS_UNKNOWN = 'UNKNOWN';

    const STATUS_CREATE = 'CREATE';
    const STATUS_CANCEL = 'CANCEL';
    const STATUS_RENEW = 'RENEW';
    const STATUS_RENEW_FAIL = 'RENEW_FAIL';

    const STATUSES = [
        self::STATUS_CREATE,
        self::STATUS_CANCEL,
        self::STATUS_RENEW,
        self::STATUS_RENEW_FAIL,
    ];

    /** @var string[] */
    protected $fillable = [
        'status',
    ];

    /** @var array */
    protected $attributes = [
        'status' => 0,
    ];

    /**
     * @return HasMany
     */
    public function transactions(): HasMany
    {
        return $this->hasMany(Transaction::class);
    }

    /**
     */
    public function setStatus(string $status): self
    {
        if (!in_array($status, self::STATUSES, true)) {
            $this->status = self::STATUS_UNKNOWN;
        }

        $this->status = $status;

        return $this;
    }

    /**
     * @param string $expiresAt
     * @return self
     */
    public function setExpiresAt(string $expiresAt): self
    {
        $this->expires_at = $expiresAt;

        return $this;
    }
}
