<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Transaction extends Model
{
    use HasFactory;

    /**
     * @return BelongsTo
     */
    public function transactions(): BelongsTo
    {
        return $this->belongsTo(Subscription::class);
    }

    /**
     * @param int $transactionId
     * @return self
     */
    public function setTransactionId(int $transactionId): self
    {
        $this->transaction_id = (int) $transactionId;

        return $this;
    }

    /**
     * @return string
     */
    public function getTransactionId(): string
    {
        return $this->transaction_id;
    }

    /**
     * @param string $transactionType
     * @return self
     */
    public function setType(string $transactionType): self
    {
        $this->type = $transactionType;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $expiresAt
     * @return self
     */
    public function setExpiresAt(string $expiresAt): self
    {
        $this->expires_at = $expiresAt;

        return $this;
    }

    /**
     * @return string
     */
    public function getExpiresAt(): string
    {
        return $this->expires_at;
    }
}
