<?php

namespace App\PaymentProviders;

use App\Interfaces\PaymentProviderInterface;
use App\Models\Subscription;
use App\Models\Transaction;
use Illuminate\Support\Arr;

class ApplePaymentProvider implements PaymentProviderInterface
{
    const STATUS_MAP = [
        'INITIAL_BUY' => Subscription::STATUS_CREATE,
        'DID_RENEW' => Subscription::STATUS_RENEW,
        'DID_FAIL_TO_RENEW' => Subscription::STATUS_RENEW_FAIL,
        'CANCEL' => Subscription::STATUS_CANCEL,
    ];

    /**
     * @param array $data
     * @return Transaction
     * @throws \Exception
     */
    public function getTransactionFromData(array $data): Transaction
    {
        $receipt = Arr::get($data, 'unified_receipt.latest_receipt_info');
        $type = self::STATUS_MAP[$data['notification_type']];

        $transaction = new Transaction();

        return $transaction
            ->setTransactionId($receipt['original_transaction_id'])
            ->setType($type)
            ->setExpiresAt($receipt['expires_date']);
    }
}
