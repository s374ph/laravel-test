<?php

namespace App\Services;

use App\Factories\PaymentProviderFactory;
use App\Models\Subscription;
use App\Models\Transaction;

class SubscriptionService
{
    const PROVIDER_APPLE = 'apple';
    const PROVIDER_ANDROID = 'android';

    const ALLOWED_PROVIDERS = [
        self::PROVIDER_APPLE,
        self::PROVIDER_ANDROID,
    ];

    /**
     * @param PaymentProviderFactory $paymentProviderFactory
     * @param array $data
     * @throws \Exception
     */
    public function handleSubscription(PaymentProviderFactory $paymentProviderFactory, array $data)
    {
        $paymentProvider = $paymentProviderFactory->getPaymentProvider();

        $transaction = $paymentProvider->getTransactionFromData($data);

        if (!in_array($transaction->getType(), Subscription::STATUSES, true)) {
            throw new \Exception('Unsupported transaction type');
        }

        $subscription = $this->getSubscriptionByTransaction($transaction);

        if (null === $subscription && Subscription::STATUS_CREATE === $transaction->getType()) {
            $subscription = $this->createFromTransaction($transaction);
        } elseif (Subscription::STATUS_RENEW === $transaction->getType()) {
            $subscription = $this->renew($subscription, $transaction);
        } elseif (Subscription::STATUS_RENEW_FAIL === $transaction->getType()) {
            $subscription = $this->renewFail($subscription, $transaction);
        } elseif (Subscription::STATUS_CANCEL === $transaction->getType()) {
            $subscription = $this->cancel($subscription, $transaction);
        }

        //some more logic for $subscription
    }

    /**
     * @param Transaction $transaction
     * @return null|Subscription
     */
    public function getSubscriptionByTransaction(Transaction $transaction): ?Subscription
    {
        return Transaction::where('transaction_id', $transaction->getTransactionId())->first()->subscription;
    }

    /**
     * @param Transaction $transaction
     * @return Subscription
     */
    public function createFromTransaction(Transaction $transaction): Subscription
    {
        $subscription = new Subscription();
        $subscription
            ->setStatus($transaction->getType())
            ->setExpiresAt($transaction->getExpiresAt());

        return $subscription;
    }

    /**
     * @param Subscription $subscription
     * @param Transaction $transaction
     * @return Subscription
     */
    public function renew(Subscription $subscription, Transaction $transaction): Subscription
    {
        $subscription->setStatus($transaction->getType());
        $subscription->setExpiresAt($transaction->getExpiresAt());
        $subscription->save();

        return $subscription;
    }

    /**
     * @param Subscription $subscription
     * @param Transaction $transaction
     * @return Subscription
     */
    public function renewFail(Subscription $subscription, Transaction $transaction): Subscription
    {
        return $this->updateStatusAndExpiresAt($subscription, $transaction->getType(), date('Y-m-d H:i:s'));
    }

    /**
     * @param Subscription $subscription
     * @param Transaction $transaction
     * @return Subscription
     */
    public function cancel(Subscription $subscription, Transaction $transaction): Subscription
    {
        return $this->updateStatusAndExpiresAt($subscription, $transaction->getType(), $transaction->getExpiresAt());
    }

    /**
     * @param Subscription $subscription
     * @param string $status
     * @param string $expiresAt
     * @return Subscription
     */
    private function updateStatusAndExpiresAt(Subscription $subscription, string $status, string $expiresAt): Subscription
    {
        $subscription->setStatus($status);
        $subscription->setExpiresAt($expiresAt);
        $subscription->save();

        return $subscription;
    }
}
